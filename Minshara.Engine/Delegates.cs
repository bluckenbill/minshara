namespace Minshara
{
    public delegate void ComponentFactoryDelegate(EntityFactory entityFactory, EntityRef entityRef, ConfigNode componentData);
    public delegate Processor ProcessorFactoryDelegate();
}