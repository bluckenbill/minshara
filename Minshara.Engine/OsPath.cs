namespace Minshara
{
    public abstract class OsPath
    {
        protected string name;
        
        protected OsPath(string name)
        {
            this.name = name;
        }

        public abstract OsPath GetRoot();
        public abstract OsPath GetFileName();
        public abstract string GetExtension();
        public abstract OsPath ParentPath();

    }
}