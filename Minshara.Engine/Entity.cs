using System;
using System.Collections.Generic;

namespace Minshara
{
    public class Entity
    {
        public List<MessageEntry> inbox;
        public BitMask mask;
        public EntityId uid;
        public String name;
        public Guid guid = Guid.NewGuid();
        public bool dirty = false;
        public bool alive = true;
        public int liveComponents = 0;
        public List<(int, Component)> components;


        private Entity addComponent<T>(T component) where T : Component
        {
            throw new NotImplementedException();
        }

        // private Entity removeComponent<T>(World world)
        // {
        //     throw new NotImplementedException();
        // }

        public T tryGetComponent<T>() where T : Component
        {
            throw new NotImplementedException();
        }

        public T getComponent<T>() where T : Component
        {
            throw new NotImplementedException();
        }

        public bool hasComponent<T>() where T : Component
        {
            throw new NotImplementedException();
        }

        public Entity addComponent<T>() where T : Component, new()
        {
            throw new NotImplementedException();
        }

        public Entity removeComponentAt(int i)
        {
            throw new NotImplementedException();
        }

        public Entity deleteComponent<T>() where T : Component
        {
            throw new NotImplementedException();
        }

        public void onReady()
        {
            
        }

        public void markDiry(World w)
        {
            
        }

        public BitMask getMask()
        {
            throw new NotImplementedException();
        }

        public void refresh()
        {
            
        }

        public EntityId getEntityId()
        {
            throw new NotImplementedException();
        }

        public void destroy()
        {
            
        }
        
    }
}