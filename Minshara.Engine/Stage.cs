using System;

namespace Minshara
{
    public class Stage
    {
        public virtual void onFixedUpdate(Time time)
        {
            
        }

        public virtual void onVariableUpdate(Time time)
        {
            
        }

        public virtual void onRender(RenderContext rc)
        {
            
        }

        public virtual void init()
        {
            
        }

        protected Stage(string name = "unnamed")
        {
            this.name = name;
        }

        public MinsharaAPI getAPI()
        {
            return api;
        }
        
        protected InputAPI getInputAPI()
        {
            throw new NotImplementedException();
        }

        protected AudioAPI getAudioAPI()
        {
            throw new NotImplementedException();
        }
        
        protected CoreAPI getCoreAPI()
        {
            throw new NotImplementedException();
        }

        protected SystemAPI getSystemAPI()
        {
            throw new NotImplementedException();
        }
        
        protected NetworkAPI getNetworkAPI()
        {
            throw new NotImplementedException();
        }
        
        protected MovieAPI getMovieAPI()
        {
            throw new NotImplementedException();
        }
        
        protected Resources getResources()
        {
            throw new NotImplementedException();
        }
        
        protected Game getGame()
        {
            throw new NotImplementedException();
        }
        
        protected T getResource<T>(string name)
        {
            throw new NotImplementedException();
        }
        
        private void setGame(Game game)
        {
            throw new NotImplementedException();
        }
        
        private void doInit(MinsharaAPI api)
        {
            throw new NotImplementedException();
        }
        
        private void doDeInit()
        {
            throw new NotImplementedException();
        }

        private Game game;
        private string name;
        private MinsharaAPI api;
    }
    
}