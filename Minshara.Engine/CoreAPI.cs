using System;
using System.Collections.Generic;
using System.Numerics;
using System.Reflection.Metadata;

namespace Minshara
{
    public class CoreAPI : CoreAPIInternal, IMainLoopable, ILoggerSink
    {
        private int StageID;


        CoreAPI(Game game, List<string> args)
        {
            
        }

        void init()
        {
            
        }
        
        void setStage(StageID stage)
        {
            throw new NotImplementedException();
        }
        
        void setStage(Stage stage)
        {
            throw new NotImplementedException();
        }
        
        void initStage(Stage stage)
        {
            throw new NotImplementedException();
        }

        Stage getCurrentStage()
        {
            throw new NotImplementedException();
        }

        void quit()
        {
            throw new NotImplementedException();
        }
        
        Resources getResource()
        {
            throw new NotImplementedException();
        }
        
        Env getEnvironment()
        {
            throw new NotImplementedException();
        }
        
        uint getTime(CoreAPITimer timer, TimeLine tl, StopwatchAveraging.Mode mode)
        {
            throw new NotImplementedException();
        }

        void onFixedUpdate(Time time)
        {
            throw new NotImplementedException();
        }
        
        void onVariableUpdate(Time time)
        {
            throw new NotImplementedException();
        }
    
        bool isRunning()
        {
            throw new NotImplementedException();
        }

        bool transitionStage()
        {
            throw new NotImplementedException();
        }

        MinsharaAPI getAPI()
        {
            throw new NotImplementedException();
        }
        
        MinsharaStatics getStatics()
        {
            throw new NotImplementedException();
        }
        
        void onSuspend()
        {
            throw new NotImplementedException();
        }
        
        void onReloaded()
        {
            throw new NotImplementedException();
        }
        
        void onTerminatedInError(string erro)
        {
            throw new NotImplementedException();
        }
        
        int getTargetFPS()
        {
            throw new NotImplementedException();
        }
        
        void registerDefaulPlugins()
        {
            throw new NotImplementedException();
        }
        
        void registerPlugin(Plugin plugin)
        {
            throw new NotImplementedException();
        }
        
        List<Plugin> getPlugins(PluginType type)
        {
            throw new NotImplementedException();
        }

        void log(LoggerLevel level, string msg)
        {
            throw new NotImplementedException();
        }
        
        int getExitCode()
        {
            throw new NotImplementedException();
        }
        
/**************/

        private void deInit()
        {
            throw new NotImplementedException();
        }
        
        private void initResources()
        {
            throw new NotImplementedException();
        }
        private void setOutRedirect(bool appendToExisting)
        {
            throw new NotImplementedException();
        }
        
        private void doFixedUpdate(Time time)
        {
            throw new NotImplementedException();
        }
        private void doVariableUpdate(Time time)
        {
            throw new NotImplementedException();
        }
        private void doRender(Time time)
        {
            throw new NotImplementedException();
        }

        private void showComputerInfo()
        {
            throw new NotImplementedException();
        }

        private void pumpEvents(Time time)
        {
            throw new NotImplementedException();
        }
        private void pumpAudio()
        {
            throw new NotImplementedException();
        }
        
        private Dictionary<StopwatchAveraging, TimeLine> engineTimers()
        {
            throw new NotImplementedException();
        }
        private Dictionary<StopwatchAveraging, TimeLine> gameTimers()
        {
            
            throw new NotImplementedException();
        }
            
        StopwatchAveraging vsyncTimer;
        List<String> args;

        Env environment;
        Game game;
        MinsharaAPI api;
        Resources resources;

        Painter painter;
        Camera camera;
        RenderTarget screenTarget;
        Vector2i prevWindowSize = new Vector2i(-1, -1);

        Stage currentStage;
        Stage nextStage;
        bool pendingStageTransition = false;

        bool running = true;
        bool hasError = false;
        bool hasConsole = false;
        int exitCode = 0;
        RedirectStream stream;

        DevConClient devConClient;

        TreeMap<PluginType, List<Plugin>> plugins;
        HalleyStatics statics;

    }


}