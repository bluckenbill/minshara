using System;

namespace Minshara
{
    public class FamilyBindingBase
    {
        protected delegate void EntityCallback(Entity entity);

        private EntityCallback _addedCallback;
        private EntityCallback _removedCallback;
        
        protected FamilyBindingBase(BitMask readMask, BitMask writeMask)
        {
            this.readMask = readMask;
            this.writeMask = writeMask;
        }

        // protected Object getElement(uint index)
        // {
        //     throw new NotImplementedException();
        // }

        public virtual void bindFamily(World w)
        {
            throw new NotImplementedException();
        }

        protected void setFamily(Family family)
        {
            this.family = family;
        }

        protected void setOnEntitiesAdded(EntityCallback entityCallback)
        {
            _addedCallback = entityCallback;
        }

        protected void setOnEntitiesRemoved(EntityCallback entityCallback)
        {
            _removedCallback = entityCallback;
        }

        protected void onEntitiesAdded(Entity entity)
        {
            _addedCallback(entity);
        }

        protected void onEntitiesRemoved(Entity entity)
        {
            _removedCallback(entity);
        }

        protected Family family;
        private BitMask readMask;
        private BitMask writeMask;
        
    }
}