namespace Minshara
{
    public class ResourceData
    {
        
    }

    public interface IResourceLocator
    {
        MetaData GetMetadata(string resource, AssetType type);
    }

    public enum ResourceLoadPriority
    {
        Low,
        Medium,
        High
    }

    public class ResourceLoader
    {
        private MetaData _metaData;
        public ResourceLoader(IResourceLocator locator, string name, AssetType type, ResourceLoadPriority priority,
            MinsharaAPI api, Resources resources)
        {
            _metaData = locator.GetMetadata(name, type);
        }    
    }
    
}