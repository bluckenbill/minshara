namespace Minshara
{
    public interface IResourceLocationProvider
    {
        ResourceData GetData(string path, AssetType type, bool stream);
        AssetDatabase GetAssetDatabase();
        int GetPriority();
        void Purge(OsAPI osApi);

    }
    public class ResourceLocator : IResourceLocator
    {
        public MetaData GetMetadata(string resource, AssetType type)
        {
            throw new System.NotImplementedException();
        }
    }
}