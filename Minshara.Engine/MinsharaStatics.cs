using System.Threading;

namespace Minshara
{

    public class MinsharaStaticPimpl
    {
        private Logger logger;
        private Os os;

        public MinsharaStaticPimpl()
        {
            executors = new Executors();
            os = Os.createOS();
            logger = new Logger();
            executors.set(executors);
        }
        
        Executors executors;
        MThreadPool cpuThreadPool;
        MThreadPool cpuAuxThreadPool;
        MThreadPool diskIOThreadPool;
    }
    public class MinsharaStatics : MinsharaStaticPimpl
    {
        MinsharaStatics() : base()
        {
        }
    }
}