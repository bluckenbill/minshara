using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Xml.Serialization;

namespace Minshara
{
    public abstract class Processor
    {

        public Processor(List<FamilyBindingBase> families, List<int> messageTypesReceived)
        {
            
        }
        public string getName()
        {
            throw new NotImplementedException();
        }

        public void setName(string name)
        {
            throw new NotImplementedException();    
        }

        public int getEntityCount()
        {
            int count = 0;
            foreach(FamilyBindingBase bindingBase in families)
            {
                //count += bindingBase.count();
            }

            return count;
        }

        public bool tryInit()
        {
            if (!initialised)
            {
                initialised = initBase(); 
            }

            return initialised;
        }

        public ulong getNanoSecondsTaken()
        {
            throw new NotImplementedException();
        }

        public ulong getNanoSecondsTakenAvg()
        {
            throw new NotImplementedException();
        }

        public void setCollectSamples(bool collect)
        {
            collectSamples = collect;
        }
        
        protected MinsharaAPI doGetAPI()
        {
            return api;
        }

        protected World doGetWorld()
        {
            return world;  
        }
        
        protected abstract bool initBase();

        protected virtual void updateBase(Time time){}
        
        protected virtual void renderBase(RenderContext rc){}

        protected abstract void onMessageReceived(List<Message> messageList);
        
        protected static void invokeIndividual<Fam, Family>()
        {
            throw new NotImplementedException();
        }

        protected static void invokeParallel<Fam, Family>()
        {
            throw new NotImplementedException();
        }

        protected virtual void sendMessageGeneric<T>(EntityId entityId, T msg) where T : Message
        {
            throw new NotImplementedException();
        }
        
        protected virtual void invokeInit<T>(T processor) where T : Processor
        {
            
        }

        protected virtual void initialiseOnEntityAdded<F, P>(FamilyBinding<F> binding, P processor) where F : Family where P : Processor
        {
            throw new NotImplementedException();
            //bindingBase.setOnEntitiesAdded((processor) => { processor.onEntitiesAdded});
        }

        protected virtual void initialiseOnEntityRemoved<F, P>(FamilyBinding<F> binding, P processor) where F : Family where P : Processor
        {
            throw new NotImplementedException();
        }

        protected void initialiseFamilyBinding<T, V>(FamilyBinding<T> binding, V processor) where T : Family where V : Processor
        {
            initialiseOnEntityAdded<T, V>(binding, processor);
            initialiseOnEntityRemoved<T, V>(binding, processor);
        }

        private List<FamilyBindingBase> families;
        private List<int> messageTypesReceived;
        private List<EntityId> messagesSentTo;
        private List<(EntityId, MessageEntry)> outbox;

        private World world;
        private MinsharaAPI api;
        private string name;
        private int systemId = -1 ;
        private bool initialised = false;
        private bool collectSamples = false;

        private StopwatchAveraging timer;

        private void doUpdate(Time time)
        {
            if (collectSamples)
            {
                //timer.beginSample();
            }

            purgeMessages();
            if (messageTypesReceived.Count > 0)
            {
                processMessages();
            }
            
            updateBase(time);
            dispatchMessages();

            if (collectSamples)
            {
                //timer.endSample();
            }

        }

        private void doRender(RenderContext rc)
        {
            
        }

        private void onAddedToWorld(World w, int id)
        {
            world = w;
            systemId = id;
            
            foreach (FamilyBindingBase familyBindingBase in families)
            {
                familyBindingBase.bindFamily(world);
            }
        }

        private void purgeMessages()
        {
            if (messagesSentTo.Count > 0)
            {
                foreach (EntityId target in messagesSentTo)
                {
                    Entity entity = world.tryGetEntity(target);

                    entity?.inbox.RemoveAll(x => x.age == systemId);
                }
                messagesSentTo.Clear();
            }
        }
        struct MessageBox
        {
            public List<Message> msg;
           
        }
        private void processMessages()
        {
            if (families.Count > 0)
            {
                FamilyBindingBase fam = families[0];
                foreach (FamilyBindingBase familyBindingBase in families)
                {
                    // FamilyBase elem =  familyBindingBase;
                    // Entity entity = world.tryGetEntity(elem.entityId);
                    throw new NotImplementedException();
                }
            }
        }

        private void doSendMessage(EntityId target, Message msg, int msgId)
        {
            
        }

        private void dispatchMessages()
        {
            
        }





    }
}