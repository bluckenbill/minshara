namespace Minshara
{
    public abstract class Component
    {
        private readonly int _id;

        protected Component(int id)
        {
            _id = id;
        }

        public int getId()
        {
            return _id;
        }

        public abstract void deserialize(Resources resources, ConfigNode node);
    }
}