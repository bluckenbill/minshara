using System;
using System.Collections.Generic;
using Microsoft.VisualBasic.CompilerServices;

namespace Minshara
{
    public abstract class Family
    {
        //src/engine/entity/include/halley/entity/family.h
        public BitMask mask;
        protected List<Family> _families;
        public static int read;
        
        protected Family(BitMask familyBitMask)
        {
            
        }

        protected Family()
        {
            bool result = initialize();
            if (mask == null || result == false) throw new Exception("Not initialized.");
        }

        private bool initialize()
        {
            bool result = false;


            return result;
        } 

        public void addOnEntitiesAdded(FamilyBindingBase familyBindingBase)
        {
            
        }

        public void removeOnEntityAdded(FamilyBindingBase familyBindingBase)
        {
            
        }

        public void addOnEntitiesRemoved(FamilyBindingBase familyBindingBase)
        {
            
        }

        public void removeOnEntityRemoved(FamilyBindingBase familyBindingBase)
        {
            
        }

        public Family GetFamily(int index)
        {
            return _families[index];
        }
        
        public virtual Family addEntity(Entity entity)
        {
            throw new NotImplementedException();
        }
        
        public Family removeEntity(Entity entity)
        {
            throw new NotImplementedException();
        }        
        
        public void removeDeadEntities()
        {
            
        }

        public virtual void updateEntities()
        {
            
        }

        public virtual void clearEntities()
        {
            
        }


        
        //


        public void notifyAdd(List<Entity> entities)
        {
            
        }

        public void notifyRemoved(List<Entity> entities)
        {
            
        }



        public static FamilyMaskType inclusionMask()
        {
          throw new NotImplementedException();  
        }
    }


    

}
