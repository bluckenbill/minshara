using System;

namespace Minshara
{
    public class EntityStage : Stage
    {

        
        public World CreateWorld(string configName, ComponentFactoryDelegate componentFactoryDelegate, ProcessorFactoryDelegate processorFactoryDelegate)
        {
            World result = new World(getAPI(), getGame().isDevMode(), componentFactoryDelegate);
            ConfigNode config = getResource<ConfigFile>(configName);
            
            result.loadProcessors(config, processorFactoryDelegate);

            return result;
        }
    }
}