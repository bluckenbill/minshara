using System.Collections;

namespace Minshara
{
    public class RealType
    {
        public BitArray bitset = new BitArray(256);

        public BitArray getRealValue()
        {
            return bitset;
        }
    }
}