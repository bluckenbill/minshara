using System;

namespace Minshara
{
    public static class ConfigNodeDeserializer<T>
    {
        public static T deserialize(Resources resources, ConfigNode node)
        {
            object result;
            if (typeof(T) == typeof(int))
            {
                result = 2;
            } 
            else if (typeof(T) == typeof(float))
            {
                result = 0.1;
            } 
            else if (typeof(T) == typeof(string))
            {
                result = "a string";
            }
            else
            {
                throw new Exception("unable to deserialize type: " + typeof(T).FullName);
            }

            return (T) result;
        } 
    }
    
    public class ConfigNodeHelper
    {
        public static void deserializeIfDefined<T>(out T dst, Resources resources, ConfigNode node)
        {
            dst = ConfigNodeDeserializer<T>.deserialize(resources, node);
        }
    }
}