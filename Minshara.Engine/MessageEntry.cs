using System;
using System.Collections.Generic;

namespace Minshara
{
    public abstract class MessageEntry
    {
        public int type = -1;
        public int age = -1;

        public Message msg;

        public MessageEntry()
        {
            throw new NotImplementedException();
        }

        public MessageEntry(Message msg, int type, int age)
        {
            this.msg = msg;
            this.type = type;
            this.age = age;
        }
    }
}