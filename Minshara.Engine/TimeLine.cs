namespace Minshara
{
    public enum TimeLine
    {
        FixedUpdate,
        VariableUpdate,
        Render,
        NumberOfTimelines
    }
}