namespace Minshara
{
    public class FamilyBinding<T> : FamilyBindingBase where T : Family
    {
        protected FamilyBinding(BitMask readMask, BitMask writeMask) : base(readMask, writeMask)
        {
            
        }

        public Family getElement(int index)
        {
            return family.GetFamily(index);
        }
        
        public override void bindFamily(World w)
        {
            setFamily(w.getFamily<T>());
        }
    }
}