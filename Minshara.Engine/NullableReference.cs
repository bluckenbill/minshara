using System;
using System.Threading;

namespace Minshara
{
    public class NullableReference
    {
        public NullableReference(NullableReference other)
        {
            
        }

        public NullableReference()
        {
            throw  new NotImplementedException();
        }

        protected NullableReferenceAnchor getRaw()
        {
            return reference;
        }
        
        private NullableReferenceAnchor reference;
        private NullableReference previous;
        private NullableReference next;

        public NullableReference(NullableReferenceAnchor anchor)
        {
            throw new NotImplementedException();
        }

        private void nullify()
        {
            throw new NotImplementedException();
        }

        private void setReference(NullableReferenceAnchor anchor)
        {
            throw new NotImplementedException();
        }
    }
    
    public class NullableReferenceOf<T> : NullableReference where T : class
    {
        public NullableReferenceOf()
        {
            
        }

        public NullableReferenceOf(NullableReference other)
        {
            throw new NotImplementedException();
        }

        public NullableReferenceOf(NullableReferenceOf<T> other)
        {
            throw new NotImplementedException();
        }
        
        
        
        T get()
        {
            return getRaw() as T;
        }
    }
    public class NullableReferenceAnchor
    {
        public NullableReferenceAnchor()
        {
            
        }

        public NullableReferenceAnchor(NullableReferenceAnchor other)
        {
            throw new NotImplementedException();
        }

        public NullableReference getReference()
        {
            throw new NotImplementedException();
        }

        public NullableReferenceOf<T> getReferenceOf<T>() where T : class
        {
            return new NullableReferenceOf<T>(getReference());
        }

        private NullableReference firstReference;

        private void addReference(NullableReference reference)
        {
            throw new NotImplementedException();
        }
    }
    
    
}