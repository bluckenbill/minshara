using System;

namespace Minshara
{
    namespace MinsharaAPIFlags
    {
        public enum MinsharaAPIFlags
        {
            Video = 0b_0000_0001,
            Audio = 0b_0000_0010,
            Input = 0b_0000_0100,
            Network = 0b_0000_1000,
            Platform = 0b_0001_000,
            Movie = 0b_0010_0000
        }
        
    }

    public class MinsharaAPI
    {
        // Beware of member order
        private CoreApiInternal coreInternal;
        private SystemAPIInternal systemInternal;
        private VideoAPIInternal videoInternal;
        private InputAPIInternal inputInternal;
        private AudoAPIInternal audioInternal;
        private AudioOutputAPIInternal audioOutputInternal;
        private PlatformAPIInternal platformInternal;
        private NetworkAPIInternal networkInternal;
        private MovieAPIInternal movieInternal;

        private CoreAPI core;
        private SystemAPI system;
        private VideoAPI video;
        private InputAPI input;
        private AudioAPI audio;
        private AudioOutputAPI adioOutput;
        private PlatformAPI platform;
        private NetworkInternalAPI network;
        private MovieAPI movie;

        void init()
        {
            throw new NotImplementedException();
        }
        

        void deInit()
        {
            throw new NotImplementedException();
        }
        
        void assign()
        {
            throw new NotImplementedException();
        }
    }

    internal class NetworkInternalAPI
    {
    }



    internal class MovieAPIInternal
    {
    }

    internal class NetworkAPIInternal
    {
    }

    internal class PlatformAPIInternal
    {
    }

    internal class AudioOutputAPIInternal
    {
    }

    internal class AudoAPIInternal
    {
    }

    internal class InputAPIInternal
    {
    }

    internal class VideoAPIInternal
    {
    }

    internal class SystemAPIInternal
    {
    }

    internal class CoreApiInternal
    {
    }
}