using System;

namespace Minshara
{
    public class FamilyBaseOf<T> : FamilyBase where T : Family 
    {
        protected FamilyBaseOf()
        {
            throw new NotImplementedException();
        }
        public NullableReferenceOf<T> getReference()
        {
            return anchor.getReferenceOf<T>();
        }
        
    }
}