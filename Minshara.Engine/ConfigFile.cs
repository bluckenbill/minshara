using System;

namespace Minshara
{
    enum ConfigNodeType
    {
        Undefined,
        String,
        Sequence,
        Map,
        Int,
        Float,
        Double,
        Bytes
    }
    
    public class ConfigNode
    {
        public ConfigNode Get(string name)
        {
            throw new NotImplementedException();
        }
    }

    public class ConfigFile : ConfigNode
    {
    }
}