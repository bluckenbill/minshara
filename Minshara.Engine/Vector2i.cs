namespace Minshara
{
    public class Vector2i
    {
        public int X { get; set; }
        private int Y { get; set; }
        
        public Vector2i(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}