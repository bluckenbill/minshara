namespace Minshara
{
    public abstract class OsAPI
    {
        public abstract OsPath GetAssetsPath(OsPath gamePath);
    }
}