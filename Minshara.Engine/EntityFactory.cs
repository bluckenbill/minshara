
using System;

namespace Minshara
{
    public class EntityFactory
    {
        private Resources _resouces;
        private World _world;
        private ConfigNodeSerializationContext _context;

        EntityRef createEntity(ConfigNode node)
        {
            throw new NotImplementedException();
        }

        void updateEntity(EntityRef entityRef, ConfigNode node, bool transformOnly = false)
        {
            
        }

        void updateEntityTree(EntityRef entity, ConfigNode node)
        {
            
        }

        EntityRef createEntity(EntityRef parent, ConfigNode node, bool populate)
        {
            throw new NotImplementedException();
        }
        
        
        public EntityFactory(World world, Resources resources)
        {
            _world = world;
            _resouces = resources;
        }

        public void CreateComponent<T>(EntityRef entity, ConfigNode componentData) where T : Component, new()
        {
            T comp = entity.tryGetComponent<T>();
            if (comp != null)
            {
                comp.deserialize(_resouces, componentData);
            }
            else
            {
                T component = new T();
                component.deserialize(_resouces, componentData);
                entity.addComponent<T>(component, 0);
            }
        }
        
    }
}