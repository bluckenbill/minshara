using System;
using System.Collections.Generic;

namespace Minshara
{
    public class Game
    {
        public virtual void init(Env environment, List<String> args)
        {
            throw new NotImplementedException();
        }
        public virtual int initPlugins(IPluginRegistry registry)
        {
            throw new NotImplementedException();
        }
        
        public virtual void initResourceLocator(OsPath gamePath, OsPath assetsPath, OsPath unpackedAssetsPath, ResourceLocator locator)
        {
            throw new NotImplementedException();
        }

        public virtual String getName()
        {
            throw new NotImplementedException();
        }
        public virtual String getDataPath()
        {
            throw new NotImplementedException();
        }
        public virtual bool isDevMode()
        {
            throw new NotImplementedException();
        }
        public virtual bool shouldCreateSeparateConsole() { return isDevMode(); }

        public virtual Stage startGame(MinsharaAPI api)
        {
            throw new NotImplementedException();
        }
        
        public virtual void endGame()
        {
            throw new NotImplementedException();
        }

        public virtual Stage makeStage(StageID StageID)
        {
            throw new NotImplementedException();
        }

        public virtual int getTargetFPS() { return 60; }

        public virtual String getDevConAddress() { return ""; }
        public virtual int getDevConPort() { return 12500; }

        public virtual GameConsole getGameConsole() 
        {
            throw new NotImplementedException();
        }
		
        public virtual void onUncaughtException(Exception exception, TimeLine timeLine)
        {
            throw new NotImplementedException();
        }
    }
}