using System;
using System.Collections.Generic;
using System.Threading;

namespace Minshara
{
    public class World
    {
        public World(MinsharaAPI api, bool isDevMode, ComponentFactoryDelegate componentFactoryDelegate)
        {
            
        }
        
        public void step(TimeLine timeline, Time elapsed)
        {
            throw new NotImplementedException();
        }

        public void render(RenderContext rc)
        {
            throw new NotImplementedException();
        }

        public bool hasProcessorsOnTimeLine(TimeLine timeLine)
        {
            throw new NotImplementedException();
        }

        public uint getAverageTime(TimeLine timeLine)
        {
            throw new NotImplementedException();
        }
        
        
        
        public Processor addProcessor(Processor system)
        {
            throw new NotImplementedException();
        }

        public void removeProcessor(Processor system)
        {
            throw new NotImplementedException();    
        }

        public List<Processor> getProcessors()
        {
            throw new NotImplementedException();
        }
        
        public List<Processor> getProcessors(TimeLine timeLine)
        {
            throw new NotImplementedException();
        }        

        public Processor getProcessor(string names)
        {
            throw new NotImplementedException();
        }        
        
        public Processor getProcessor<T>()
        {
            throw new NotImplementedException();
        }

        public Service addService(Service service)
        {
            throw new NotImplementedException();
        }

        public void loadProcessors(ConfigNode root, ProcessorFactoryDelegate processorFactoryDelegate)
        {
            throw new NotImplementedException();
        }

        public T getService<T>() where T : Service
        {
            throw new NotImplementedException();
        }
        
        public T getService<T>(string name) where T : Service
        {
            throw new NotImplementedException();
        }
        
        public EntityRef createEntity(Guid guid, string name)
        {
            throw new NotImplementedException();
        }

        public EntityRef createEntity(string name)
        {
            throw new NotImplementedException();
        }

        public void destroyEntity(int entityId)
        {
            
        }

        public EntityRef getEntity(int entityId)
        {
            throw new NotImplementedException();
        }
        
        public Entity tryGetEntity(EntityId entityId)
        {
            throw new NotImplementedException();
        }

        public uint numEntities()
        {
            throw new NotImplementedException();
        }

        public List<EntityRef> getEntities()
        {
            throw new NotImplementedException();
        }

        public void spawnPending()
        {
            throw new NotImplementedException();
        }

        public void onEntityDiry()
        {
            throw new NotImplementedException();
        }

        public T getFamily<T>() where T : Family
        {
            
            throw  new NotImplementedException();
            
        }

        private MinsharaAPI api;
        private List<Dictionary<Processor, int>> systems;
        private ComponentFactoryDelegate createComponent;
        private bool collectMetrics = false;
        private bool entityDirty = false;
        private List<Entity> entities;
        private List<Entity> entitiesPendingCreation;
        private Dictionary<EntityId, Entity> entityMap;

        private List<Family> families;
        private Dictionary<FamilyType, List<Family>> familyCache;

        private List<StopwatchAveraging> timer;

        private void allocateEntity(Entity entity)
        {
            throw new NotImplementedException();
        }

        private void updateEntities()
        {
            throw new NotImplementedException();
        }
        
        
        
        private void initSystems()
        {
            throw new NotImplementedException();
        }

        private void doDestroyEntity(EntityId id)
        {
            throw new NotImplementedException();
        }

        private void deleteEntity(Entity entity)
        {
            throw new NotImplementedException();
        }

        private void updateProcessors(TimeLine timeLine, double deltaTime)
        {
            throw new NotImplementedException();
            // Get processors
            
            //run update on the processors
        }

        private void renderProcessors(RenderContext rc)
        {
            throw new NotImplementedException();
        }

        public void onAddFamily(Family family)
        {
            throw new NotImplementedException();
        }

        private Service getService(string name)
        {
            throw new NotImplementedException();
        }
        
        private List<Family> getFamiliesFor(FamilyMaskType mask)
        {
            throw new NotImplementedException();
        }


        
    }
}