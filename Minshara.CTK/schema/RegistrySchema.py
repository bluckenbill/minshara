from jinja2 import FileSystemLoader, Environment


class RegistrySchema:
    def __init__(self, component_list, processor_list):
        self.component_list = component_list
        self.processor_list = processor_list

    def render(self, template_path):
        file_loader = FileSystemLoader(template_path)
        env = Environment(loader=file_loader)
        template = env.get_template('registry.jinja2')

        output = template.render(registry=self)
        return output
