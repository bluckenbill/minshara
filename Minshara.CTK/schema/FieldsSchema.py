from enum import Enum
from typing import List


class MemberAccess(Enum):
    Public = 'public'
    Protected = 'protected'
    Private = 'private'


class MemberType(Enum):
    Bool = 'bool'
    Char = 'char'
    Int = 'int'
    Double = 'double'
    Float = 'float'
    String = 'string'
    Custom = 'custom'


class TypeSchema:
    def __init__(self, name: str, is_const: bool = False, is_static: bool = False, is_const_expr: bool = False):
        self.name = name
        self.isConst = is_const
        self.isStatic = is_static
        self.isConstExpr = is_const_expr

        if self.name is "bool":
            self.member_type = MemberType.Bool
        elif self.name is "char":
            self.member_type = MemberType.Char
        elif self.name is "int":
            self.member_type = MemberType.Int
        elif self.name is "double":
            self.member_type = MemberType.Double
        elif self.name is "float":
            self.member_type = MemberType.Float
        elif self.name is "string":
            self.member_type = MemberType.String
        else:
            self.member_type = MemberType.Custom

    def __str__(self):
        return_value = ""

        if self.isConst:
            return_value = return_value + "const "
        if self.isStatic:
            return_value = return_value + "static "

        return_value = return_value + self.name

        return return_value


class VariableSchema:
    def __init__(self, variable_type: TypeSchema, name: str, initial_value: str = ''):
        self.type_schema = variable_type
        self.name = name
        self.initialValue = initial_value


class MemberSchema:
    def __init__(self, type_schema: TypeSchema, name: str, default_value: str = '', member_access: MemberAccess = MemberAccess.Public):
        self.name = name
        self.type_schema = type_schema
        self.member_access = member_access
        self.default_value = default_value

    @staticmethod
    def to_variable_schema(member_schema_list):
        result = []

        for schema in member_schema_list:
            variable_schema = VariableSchema(schema.type_schema.name, schema.name)
            result.append(variable_schema)

        return result

    def __str__(self):
        if self.default_value is None:
            return_value = self.member_access + " " \
                           + str(self.type_schema) + " " \
                           + self.name
        else:
            return_value = self.member_access + " " \
                           + str(self.type_schema) + " " \
                           + self.name + " = "

            if self.type_schema.member_type is MemberType.String:
                return_value = return_value + '"' + str(self.default_value) + '"'
            elif self.type_schema.member_type is MemberType.Char:
                return_value = return_value + "'" + str(self.default_value) + "'"
            else:
                return_value = return_value + str(self.default_value)

        return return_value


class MethodSchema:
    def __init__(self, return_type: TypeSchema, arguments: List[VariableSchema], name: str,
                 is_const: bool = False, is_final: bool = False, is_virtual: bool = False):
        self.return_type = return_type
        self.arguments = arguments
        self.name = name
        self.is_const = is_const
        self.is_final = is_final
        self.is_virtual = is_virtual
