from jinja2 import FileSystemLoader, Environment
from schema.FieldsSchema import MemberSchema, TypeSchema
from random import seed
import requests
from random import randint


class ComponentSchema:
    counter = 1

    def __init__(self, node, generate=True):
        self.node = node
        self.member_schemas = []
        self.include_files = []
        self.custom_implementation = ""

        self.name = self.node['name'].capitalize()

        seed(42069)
        self.ID = ComponentSchema.counter
        ComponentSchema.counter *= 2

        members_node = node.get('members', [])
        for member in members_node:
            name, value = member.popitem()
            type_schema = TypeSchema(value)
            member_schema = MemberSchema(type_schema, name)
            self.member_schemas.append(member_schema)

    def render(self, template_path):
        file_loader = FileSystemLoader(template_path)
        env = Environment(loader=file_loader)
        template = env.get_template('component.jinja2')

        output = template.render(component=self)
        return output


    def __str__(self):
        return str(self.node)

class ComponentFake:
    def __init__(self,name,id):
        self.ID = id
        self.name = name

class ComponentFlagsSchema:
    def __init__(self, component_schema_list):
        self.component_schema_list = component_schema_list

    def render(self, template_path):
        file_loader = FileSystemLoader(template_path)
        env = Environment(loader=file_loader)

        word_site = "http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain"

        response = requests.get(word_site)
        WORDS = response.content.splitlines()
        self.component_schema_list.clear()
        count = 1
        for word in range(200):
            count *= 2
            item = ComponentFake(str(WORDS[word].decode('UTF-8')), count)
            self.component_schema_list.append(item)


        template = env.get_template('component_flags.jinja2')

        output = template.render(ComponentFlagsSchema=self)
        return output
