from codegen.CodeGenLanguage import CodeGenLanguage
from jinja2 import FileSystemLoader, Environment
from enum import Enum
from typing import List


class ProcessorStrategy(Enum):
    Global = 'global'
    Individual = 'individual'
    Parallel = 'parallel'


class ProcessorAccess(Enum):
    Pure = 'pure'
    Api = 'api'
    World = 'world'
    Resources = 'resources'


class ProcessorMethod(Enum):
    Update = 'update'
    Render = 'render'


class ComponentReferenceSchema:
    def __init__(self, name: str, write: bool = False, optional: bool = False):
        self.name = name
        self.write = write
        self.optional = optional


class MessageReferenceSchema:
    def __init__(self, name: str, send: bool = False, receive: bool = False):
        self.name = name
        self.send = send
        self.receive = receive


class FamilySchema:
    def __init__(self, name: str, component_schemas: List[ComponentReferenceSchema]):
        self.name = name
        self.component_schemas = component_schemas


class ServiceSchema:
    def __init__(self, name: str):
        self.name = name


class ProcessorSchema:
    def __init__(self, node, generate=False):
        self.node = node
        self.language = CodeGenLanguage.CSharp
        self.families = []
        self.access = []
        self.services = []
        self.messages = []

        self.name = node.get('name')

# Families
        for family in node.get('families', []):
            fam_name, components = family.popitem()
            component_refs = []
            for component in components:
                comp_name, value = component.popitem()
                if value == 'write':
                    comp_ref = ComponentReferenceSchema(comp_name, True, False)
                    component_refs.append(comp_ref)
                elif value == 'read':
                    comp_ref = ComponentReferenceSchema(comp_name, False, False)
                    component_refs.append(comp_ref)
                else:
                    raise

            family_schema = FamilySchema(fam_name, component_refs)

            self.families.append(family_schema)

# Processor Method
        method_string = node.get('method', 'update')

        self.method = ProcessorMethod[str.capitalize(method_string)]

# Processor Strategy
        num_of_families = len(self.families)
        default_strategy = 'global' if num_of_families is 0 else 'individual'
        strategy_str = str.capitalize(node.get('strategy', default_strategy))

        self.strategy = ProcessorStrategy[strategy_str]

# Processor Access
        access_list = node.get('access', [])
        for access in access_list:
            access_string = str.capitalize(access)
            access_enum = ProcessorAccess[access_string]

            self.access.append(access_enum)


# Processor Services
        for service in node.get('services', []):
            service_schema = ServiceSchema(service)

            self.services.append(service_schema)

# Processor Messages
        for message in node.get('messages', [{}]):
            name, value = message.popitem()
            is_send = False
            is_receive = False

            if value == 'send':
                is_send = True
            elif value == 'receive':
                is_receive = True
            else:
                raise

            message = MessageReferenceSchema(name, is_send, is_receive)

            self.messages.append(message)

    def can_receive_message(self):
        can = False

        for message in self.messages:
            if message.receive:
                can = True

        return can

    def render(self, template_path):
        file_loader = FileSystemLoader(template_path)
        env = Environment(loader=file_loader, trim_blocks=True)
        template = env.get_template('processor.jinja2')

        output = template.render(processor=self)
        return output

    def __str__(self):
        return str(self.node)