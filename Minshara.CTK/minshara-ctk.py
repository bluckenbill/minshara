#!/usr/bin/python3
from codegen import CodeGenerator, CodeGenSourceInfo
import sys
import getopt




def main(argv):
    input_file = ''
    output_directory = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('minshara-ctk.py -i <input_file> -o <output_directory>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('minshara-ctk.py -i <input_file> -o <output_directory>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_file = arg
        elif opt in ("-o", "--ofile"):
            output_directory = arg

    print('Input file is: ', input_file)
    print('Output directory is: ', output_directory)

    file_list = [(CodeGenSourceInfo(input_file))]

    code_generator = CodeGenerator(True)
    code_generator.load_sources(file_list)
    code_generator.generate_code(output_directory)


if __name__ == "__main__":
    main(sys.argv[1:])

