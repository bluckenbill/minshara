from schema.ComponentSchema import ComponentSchema
from schema.FieldsSchema import VariableSchema, TypeSchema
from schema.ProcessorSchema import ProcessorSchema, ProcessorMethod, ProcessorStrategy
from codegen.CodeGenLanguage import CodeGenLanguage
from codegen.CodeGenFile import CodeGenFile
from typing import List, Dict


class CodeGeneratorCSharp:
    language: CodeGenLanguage = CodeGenLanguage.CSharp
    extension = "cs"
    template_directory = 'templates/CSharp'

    @staticmethod
    def _make_path(directory, class_name, extension):
        return str(directory) + "/" + class_name + "." + extension

    @staticmethod
    def generate_component(component_schema):
        class_name = component_schema.name + "Component" + "Base"
        path = CodeGeneratorCSharp._make_path("components", class_name, "cs")
        content_rendered = component_schema.render(CodeGeneratorCSharp.template_directory)
        codegen_result = CodeGenFile(path, content_rendered)

        return codegen_result

    @staticmethod
    def generate_processor(processor_schema):
        class_name = processor_schema.name + "Processor" + "Base"
        path = CodeGeneratorCSharp._make_path("processors", class_name, "cs")
        content_rendered = processor_schema.render(CodeGeneratorCSharp.template_directory)
        codegen_result = CodeGenFile(path, content_rendered)

        return codegen_result

    @staticmethod
    def generate_registry(registry_schema):
        class_name = "RegistryBase"
        path = CodeGeneratorCSharp._make_path("registry", class_name, "cs")
        content_rendered = registry_schema.render(CodeGeneratorCSharp.template_directory)
        codegen_result = CodeGenFile(path, content_rendered)

        return codegen_result

    @staticmethod
    def generate_component_flags(component_flag_schema):
        class_name = "ComponentFlag"
        path = CodeGeneratorCSharp._make_path("flag", class_name, "cs")
        content_rendered = component_flag_schema.render(CodeGeneratorCSharp.template_directory)
        codegen_result = CodeGenFile(path, content_rendered)

        return codegen_result

    @staticmethod
    def _generate_component_header(component_schema: ComponentSchema) -> List[str]:
        return "_generate_component_header"

    @staticmethod
    def _generate_system_header(system_schema: ProcessorSchema, component_schemas: Dict[str, ComponentSchema]) -> List[str]:
        return "_generate_system_header"


class ProcessorInfo:
    def __init__(self, processor_schema):
        if processor_schema.method == ProcessorMethod.Update:
            self.method_name = "update"
            self.method_arg_type = "Time"
            self.method_arg_name = "time"
            self.method_const = False
        elif processor_schema.method == ProcessorMethod.Render:
            self.method_name = "render"
            self.method_arg_type = "RenderContext"
            self.method_arg_name = "rc"
            self.method_const = True
        else:
            raise

        self.familyArgs = []
        self.familyArgs.append(VariableSchema(TypeSchema(self.method_arg_type), self.method_arg_name))

        if processor_schema.strategy == ProcessorStrategy.Global:
            self.stratImpl = self.method_name + '(' + self.method_arg_name + ')'
