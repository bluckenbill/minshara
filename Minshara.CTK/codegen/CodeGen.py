from schema.ComponentSchema import ComponentSchema
from schema.ProcessorSchema import ProcessorSchema
from schema.ComponentSchema import ComponentFlagsSchema
from codegen.CodeGeneratorCSharp import CodeGeneratorCSharp
from typing import List
import yaml
import os

from schema.RegistrySchema import RegistrySchema


class Stats:
    def __init__(self):
        self.written: int = 0
        self.skipped: int = 0
        self.files: List[str] = []


class CodeGenSourceInfo:
    def __init__(self, file_name: str, generate: bool = False):
        self.file_name = file_name
        # self.data = data
        self.generate = generate


class CodeGenerator:
    def __init__(self, verbose):
        self.verbose = verbose
        self.components = {}
        self.processors = {}

    def load_sources(self, files: List[CodeGenSourceInfo]):
        print("Loading sources")
        self.verbose = True
        for file in files:
            self._add_source(file)

    def write_files(self, output_directory, code_gen_result):
        output_file = os.path.join(output_directory, code_gen_result.file_path)
        base_path = os.path.join(output_directory, os.path.basename(os.path.dirname(output_file)))
        print("Writing to file: " + str(output_file))
        if os.path.exists(base_path) is False:
            os.makedirs(base_path)

        with open(output_file, "w") as file:
            file.write(code_gen_result.contents)


    def generate_code(self, directory: str):
        stats = Stats()
        generators = [CodeGeneratorCSharp()]

        for generator in generators:
            component_list = []
            processor_list = []

            for k, v in self.components.items():
                print("Writing component template")
                component_code_result = generator.generate_component(v)
                self.write_files(directory, component_code_result)
                stats.written += 1
                component_list.append(v)
                stats.files.append('')

            for k, v in self.processors.items():
                print("Writing processor template")
                processor_code_result = generator.generate_processor(v)
                self.write_files(directory, processor_code_result)
                stats.written += 1
                processor_list.append(v)
                stats.files.append('')

            path = os.getcwd()
            print("The current working directory is %s" % path)
            registry_schema = RegistrySchema(component_list, processor_list)
            registry_code_result = generator.generate_registry(registry_schema)
            self.write_files(directory, registry_code_result)

            component_flag_schema = ComponentFlagsSchema(component_list)
            component_flag_result = generator.generate_component_flags(component_flag_schema)
            self.write_files(directory, component_flag_result)

        return stats.files

    def _add_source(self, source_info):
        print("Adding file: " + str(source_info.file_name))
        with open(source_info.file_name) as yaml_file:
            data = yaml.safe_load_all(yaml_file)

            for document in data:
                for key, value in document.items():
                    if key == 'component':
                        self._add_component(value, True)
                    elif key == 'processor':
                        self._add_processor(value, True)

    def _add_component(self, root_yaml_node, generate: bool):
        component = ComponentSchema(root_yaml_node, generate)
        self.components[component.name] = component

        print("Adding Component: " + component.name)

    def _add_processor(self, root_yaml_node, generate: bool):
        processor = ProcessorSchema(root_yaml_node, generate)
        self.processors[processor.name] = processor

        print("Adding System: " + processor.name)
